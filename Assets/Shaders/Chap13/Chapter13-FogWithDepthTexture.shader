﻿Shader "Unity Shaders Book/Chapter 13/Fog With Depth Texture"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        // 雾效最大密度
        _FogDensity ("Fog Density", Float) = 1.0
        // 雾效颜色
        _FogColor ("Fog Color", Color) = (1, 1, 1, 1)
        // 雾效起点高度
        _FogStart ("Fog Start", Float) = 0.0
        // 雾效终点高度
        _FogEnd ("Fog End", Float) = 1.0
    }
    SubShader
    {
        CGINCLUDE

        #include "UnityCG.cginc"

        // 记录相机到画面四个角的向量
        // （没有在Properties里声明，但是可以通过脚本传递到Shader）
        float4x4 _FrustumCornersRay;
        
        sampler2D _MainTex;
        // 主纹理的纹素大小
        half4 _MainTex_TexelSize;
        // 深度纹理
        sampler2D _CameraDepthTexture;
        // 雾效最大密度
        half _FogDensity;
        // 雾效颜色
        fixed4 _FogColor;
        // 雾效起点高度
        float _FogStart;
        // 雾效终点高度
        float _FogEnd;

        struct v2f
        {
            float4 pos : SV_POSITION;
            half2 uv : TEXCOORD0;
            half2 uv_depth : TEXCOORD1;
            float4 interpolatedRay : TEXCOORD2;
        };

        v2f vert(appdata_img v)
        {
            v2f o;
            o.pos = UnityObjectToClipPos(v.vertex);
            o.uv = v.texcoord;
            // 深度纹理UV
            o.uv_depth = v.texcoord;

            // 兼容不同平台
            // 通常Unity会对DirectX、Metal这样的平台（他们的(0,0)在左上角）的图像进行翻转，
            // 但如果这些平台开启了抗锯齿，Unity不会进行翻转
            // 所以我们需要根据纹素的正负来判定图像是否需要翻转
            #if UNITY_UV_STARTS_AT_TOP
            if (_MainTex_TexelSize.y < 0)
                o.uv_depth.y = 1 - o.uv_depth.y;
            #endif

            // 使用索引值，来获取_FrustCornersRay中对应的行作为该顶点的interpolatedRay值
            int index = 0;
            float x = v.texcoord.x;
            float y = v.texcoord.y;
            // 一般使用if会造成比较大的性能问题，但本案例中用到的模型是一个四边形网格，质保函4个顶点，所以影响不大
            if (x < 0.5 && y < 0.5)
                index = 0;
            else if (x > 0.5 && y < 0.5)
                index = 1;
            else if (x > 0.5 && y > 0.5)
                index = 2;
            else
                index = 3;
            
            // 兼容不同平台
            #if UNITY_UV_STARTS_AT_TOP
            if (_MainTex_TexelSize.y < 0)
                index = 3 - index;
            #endif

            o.interpolatedRay = _FrustumCornersRay[index];

            return o;
        }

        fixed4 frag(v2f i) : SV_Target
        {
            // 对深度纹理进行采样，并转化为视角空间下的线性深度值
            float linearDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv_depth));
            // 基于线性深度值，得到当前像素相对于相机位置的偏移，以此得到当前像素的世界位置
            float3 worldPos = _WorldSpaceCameraPos + linearDepth * i.interpolatedRay.xyz;

            // 基于y分量计算得到归一化的雾效密度
            float fogDensity = (_FogEnd - worldPos.y) / (_FogEnd - _FogStart);
            // 与雾效最大密度相乘，得到实际的雾效密度
            fogDensity = saturate(fogDensity * _FogDensity);

            // 主纹理采样
            fixed4 finalColor = tex2D(_MainTex, i.uv);
            // 根据雾效密度，在实际颜色和雾效颜色之间进行差值
            finalColor.rgb = lerp(finalColor.rgb, _FogColor.rgb, fogDensity);

            return finalColor;
        }

        ENDCG

        // 雾效Pass
        Pass
        {
            ZTest Always
            Cull Off
            ZWrite Off

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            ENDCG
        }
    }

    Fallback Off
}

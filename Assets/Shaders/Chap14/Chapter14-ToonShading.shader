﻿Shader "Unity Shaders Book/Chapter 14/Toon Shading"
{
    Properties
    {
        _Color ("Color Tint", Color) = (1.0, 1.0, 1.0, 1.0)
        _MainTex ("Main Tex", 2D) = "white" {}
        _Ramp ("Ramp Texture", 2D) = "white" {}
        _Outline ("Outline", Range(0.0, 1.0)) = 0.1
        _OutlineColor ("Outline Color", Color) = (0.0, 0.0, 0.0, 1.0)
        _Specular ("Specular", Color) = (1.0, 1.0, 1.0, 1.0)
        _SpecularScale ("Specular Scale", Range(0.0, 0.1)) = 0.01
    }

    SubShader
    {
        // 背面渲染Pass
        Pass
        {
            // 为Pass命名，方便后续复用
            NAME "OUTLINE"
            // 只渲染背面面片
            Cull Front

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            
            // 控制轮廓线宽度
            float _Outline;
            // 控制轮廓线颜色
            fixed4 _OutlineColor;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
            };

            v2f vert (a2v v)
            {
                v2f o;

                float4 worldPos = mul(UNITY_MATRIX_MV, v.vertex);
                float3 worldNormal = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
                // 将z分量设置为某个固定值，降低内凹模型的背面面片挡住正面面片的可能性
                worldNormal.z = -0.5;

                // 顶点位置沿法线方向偏移一定距离
                worldPos = worldPos + float4(normalize(worldNormal), 0) * _Outline;
                o.pos = mul(UNITY_MATRIX_P, worldPos);


                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return fixed4(_OutlineColor.rgb, 1);
            }
            ENDCG
        }

        // 正面渲染Pass
        Pass
        {
            Tags
            {
                "LightMode" = "ForwardBase"
            }

            // 只渲染正面
            Cull Back

            CGPROGRAM

            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"

            #pragma vertex vert
            #pragma fragment frag

            fixed4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            // 控制漫反射色调的渐变纹理
            sampler2D _Ramp;
            // 高光颜色
            fixed4 _Specular;
            // 控制高光反射时使用的阈值
            float _SpecularScale;

            struct a2v
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
                float3 worldNormal : TEXCOORD1;
                float3 worldPos : TEXCOORD2;
                // 声明阴影采样坐标
                SHADOW_COORDS(3)
            };

            v2f vert(a2v v)
            {
                v2f o;

                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                // 世界空间的法线方向
                o.worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
                // 世界空间的顶点位置
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

                TRANSFER_SHADOW(o);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed3 worldNormal = normalize(i.worldNormal);
                // 世界空间的光照方向
                fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
                // 世界空间的视角方向
                fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
                // 上面两个方向的中间方向，用于计算半兰伯特漫反射系数
                fixed3 worldHalfDir = normalize(worldLightDir + worldViewDir);

                // 主纹理采样
                fixed4 c = tex2D(_MainTex, i.uv);
                // 颜色叠加
                fixed3 albedo = c.rgb * _Color.rgb;

                // 环境光
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;
                
                // 光照衰减
                UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);

                // 法线、光照方向点乘，计算漫反射系数
                fixed diff = dot(worldNormal, worldLightDir);
                diff = (diff * 0.5 + 0.5) * atten;
                // 漫反射：光照颜色 * 主纹理颜色 * 漫反射色调控制
                fixed3 diffuse = _LightColor0.rgb * albedo * tex2D(_Ramp, float2(diff, diff)).rgb;

                // 高光
                fixed spec = dot(worldNormal, worldHalfDir);
                // 通过fwidth函数，让w被设置为一个很小的值
                fixed w = fwidth(spec) * 2.0;
                // 阈值控制高光边缘渐变
                float stepFactor = smoothstep(-w, w, spec + _SpecularScale - 1);
                // 最后使用step函数，为了在_SpecularScale为0时，可以完全消除高光反射的光照
                fixed3 specular = _Specular.rgb * lerp(0, 1, stepFactor) * step(0.0001, _SpecularScale);

                return fixed4(ambient + diffuse + specular, 1.0);
            }

            ENDCG
        }
    }

    Fallback "Diffuse"
}

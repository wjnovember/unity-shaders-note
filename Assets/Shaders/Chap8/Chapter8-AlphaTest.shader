﻿// 为Shader命名
Shader "Unity Shaders Book/Chapter 8/Alpha Test"
{
    Properties
    {
        _Color ("Main Tint", Color) = (1, 1, 1, 1)
        _MainTex ("Main Tex", 2D) = "white" {}
        // 透明度测试的阈值
        _Cutoff ("Alpha Cutoff", Range(0, 1)) = 0.5
    }
    SubShader
    {
        // 透明度测试常用的三个标签
        Tags {
            // 设置渲染队列为透明度测试队列
            "Queue" = "AlphaTest"
            // 让当前Shader不会受到投射器的影响
            "IgnoreProjector" = "True"
            // 让Unity把当前Shader归入到提前定义的TransparentCutout组
            "RenderType" = "TransparentCutout"
        }

        Pass
        {
            // 设置LightMode为ForwardBase，可以让我们得到
            // 一些正确的Unity内置光照变量，如：_LightColor0
            Tags { "LightMode" = "ForwardBase" }

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            // 引入内置光照变量
            #include "Lighting.cginc"

            fixed4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed _Cutoff;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 worldNormal : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
                float2 uv : TEXCOORD2;
            };

            v2f vert (a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed3 worldNormal = normalize(i.worldNormal);
                fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));

                fixed4 texColor = tex2D(_MainTex, i.uv);

                // 透明度测试：透明度小于_Cutoff的值，则当前片元会被丢弃
                clip(texColor.a - _Cutoff);

                // 等同于
                //if ((texColor.a - _Cutoff) < 0.0)
                //{
                //      // 剔除当前片元
                //    discard;
                //}

                fixed3 albedo = texColor.rgb * _Color.rgb;
                
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;

                // 使用兰伯特定律计算漫反射光照
                fixed3 diffuse = _LightColor0.rgb * albedo
                        * max(0, dot(worldNormal, worldLightDir));

                return fixed4(ambient + diffuse, 1.0);
            }
            ENDCG
        }
    }

    // 使用内置的VertexLit Shader兜底
    Fallback "Transparent/Cutout/VertexLit"
}

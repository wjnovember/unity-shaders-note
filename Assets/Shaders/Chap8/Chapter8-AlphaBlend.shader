﻿// 修改Shader命名
Shader "Unity Shaders Book/Chapter 8/Alpha Blend"
{
    Properties
    {
        _Color ("Main Tint", Color) = (1, 1, 1, 1)
        _MainTex ("Main Tex", 2D) = "white" {}
        // 用于在透明纹理的基础上控制整体的透明度
        _AlphaScale ("Alpha Scale", Range(0, 1)) = 1
    }
    SubShader
    {
        // 透明度混合常用的3个标签
        Tags {
            // 将渲染队列修改为透明混合队列
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            // 将当前Shader归入Transparent组
            "RenderType" = "Transparent"
        }

        Pass
        {
            Tags { "LightMode" = "ForwardBase" }

            // 关闭深度写入
            ZWrite Off
            // 将源颜色（该片元着色器产生的颜色）的混合因子设为SrcAlpha
            // 将目标颜色（已存在颜色缓冲中的颜色）的混合因子设为OneMinusSrcAlpha
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "Lighting.cginc"

            fixed4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            // 声明透明度控制属性
            fixed _AlphaScale;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 worldNormal : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
                float2 uv : TEXCOORD2;
            };

            v2f vert (a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed3 worldNormal = normalize(i.worldNormal);
                fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));

                fixed4 texColor = tex2D(_MainTex, i.uv);

                // 这里移除了深度测试的代码

                fixed3 albedo = texColor.rgb * _Color.rgb;
                
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;

                fixed3 diffuse = _LightColor0.rgb * albedo
                        * max(0, dot(worldNormal, worldLightDir));

                // 设置透明通道
                return fixed4(ambient + diffuse, texColor.a * _AlphaScale);
            }
            ENDCG
        }
    }

    // 修改兜底的Shader
    Fallback "Transparent/VertexLit"
}

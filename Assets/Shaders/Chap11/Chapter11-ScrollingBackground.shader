﻿Shader "Unity Shaders Book/Chapter 11/Scrolling Background"
{
    Properties
    {
        // 第一层（较远）的背景纹理
        _MainTex ("Base Layer (RGB)", 2D) = "white" {}
        // 第二层（较近）的背景纹理
        _DetailTex("2nd Layer (RGB)", 2D) = "white" {}
        // 第一层背景纹理的水平滚动速度
        _ScrollX ("Base Layer Scroll Speed", Float) = 1.0
        // 第二层背景纹理的水平滚动速度
        _Scroll2X ("2nd Layer Scroll Speed", Float) = 1.0
        // 控制纹理的整体亮度
        _Multiplier ("Layer Multiplier", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                // uv使用float4类型，存储两张纹理的采样
                float4 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _DetailTex;
            float4 _DetailTex_ST;
            float _ScrollX;
            float _Scroll2X;
            float _Multiplier;

            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                // frac内置函数：返回数值的小数部分
                o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex) + frac(float2(_ScrollX, 0.0) * _Time.y);
                o.uv.zw = TRANSFORM_TEX(v.texcoord, _DetailTex) + frac(float2(_Scroll2X, 0.0) * _Time.y);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 firstLayer = tex2D(_MainTex, i.uv.xy);
                fixed4 secondLayer = tex2D(_DetailTex, i.uv.zw);

                // 根据第二层纹理的透明度值，得到两层颜色的叠一起后的颜色
                fixed4 c = lerp(firstLayer, secondLayer, secondLayer.a);
                c.rgb *= _Multiplier;

                return c;
            }
            ENDCG
        }
    }

    Fallback "VertexLit"
}
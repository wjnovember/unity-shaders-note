﻿Shader "Unity Shaders Book/Chapter 11/Image Sequence Animation"
{
    Properties
    {
        _Color ("Color Tint", Color) = (1, 1, 1, 1)
        // 关键帧图集
        _MainTex ("Image Sequence", 2D) = "white" {}
        // 图集横向关键帧数量
        _HorizontalAmount ("Horizontal Amount", Float) = 4
        // 图集纵向关键帧数量
        _VerticalAmount ("Vertical Amount", Float) = 4
        // 控制序列帧动画的播放速度
        _Speed ("Speed", Range(1, 100)) = 30
    }
    SubShader
    {
        // 序列帧通常是透明纹理，需要设置透明队列
        Tags {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType"="Transparent"
        }

        Pass
        {
            Tags {
                "LightMode" = "ForwardBase"
            }

            // 关闭深度写入
            ZWrite Off
            // 设置混合模式
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            half4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _HorizontalAmount;
            float _VerticalAmount;
            float _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // _Time.y自场景加载后经过的时间
                float time = floor(_Time.y * _Speed);
                float row = floor(time / _HorizontalAmount);
                float column = time - row * _VerticalAmount;

                //half2 uv = float2(i.uv.x / _HorizontalAmount, i.uv.y / _VerticalAmount);
                //uv.x += column / _HorizontalAmount;
                //uv.y -= row / _VerticalAmount;

                half2 uv = i.uv + half2(column, -row);
                uv.x /= _HorizontalAmount;
                uv.y /= _VerticalAmount;

                fixed4 c = tex2D(_MainTex, uv);
                c.rgb *= _Color;

                return c;


                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }
    }

    Fallback "Transparent/VertexLit"
}
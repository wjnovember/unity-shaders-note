﻿Shader "Unity Shaders Book/Chapter 11/Water"
{
    Properties
    {
        // 河流纹理
        _MainTex ("Texture", 2D) = "white" {}
        // 整体颜色
        _Color ("Color Tint", Color) = (1, 1, 1, 1)
        // 波动幅度
        _Magnitude ("Distortion Magnitude", Float) = 1
        // 波动频率
        _Frequency ("Distortion Frequency", Float) = 1
        // 波长倒数（数值越大，波长越小）
        _InvWaveLength ("Distortion Inverse Wave Length", Float) = 10
        // 波动移动速度
        _Speed ("Speed", Float) = 0.5
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            // 批处理会合并所有有关的模型，这些模型各自的模型空间会丢失，
            // 本例中需要在模型空间下对顶点进行偏移，所以需要禁止批处理
            "DisableBatching" = "True"
        }

        Pass
        {
            //>> 为了让水流的每个面都能显示
            Tags {"LightMode" = "ForwardBase"}
            // 关闭深度写入
            ZWrite Off
            // 开启并设置混合模式
            Blend SrcAlpha OneMinusSrcAlpha
            // 关闭剔除
            Cull Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed3 _Color;
            float _Magnitude;
            float _Frequency;
            float _InvWaveLength;
            float _Speed;

            v2f vert (appdata v)
            {
                v2f o;

                float4 offset;
                // 只对顶点的x方向进行位移
                offset.yzw = float3(0.0, 0.0, 0.0);
                // 利用_Frequency属性和内置的_Time.y变量来控制正弦函数的频率
                // 为了让不同位置具有不同的位移，另外加上模型空间下的位置分量，并乘以_InvWaveLength来控制波长
                // 最后乘以_Magnitude来控制幅度
                offset.x = sin(_Frequency * _Time.y + v.vertex.x * _InvWaveLength + v.vertex.y * _InvWaveLength + v.vertex.z * _InvWaveLength) * _Magnitude;
                o.pos = UnityObjectToClipPos(v.vertex + offset);

                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                // 使用_Time.y和_Speed来控制水平方向上的纹理动画
                o.uv += float2(0.0, _Time.y * _Speed);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 c = tex2D(_MainTex, i.uv);
                c.rgb *= _Color.rgb;
                return c;
            }
            ENDCG
        }
    }

    Fallback "Transparent/VertexLit"
}

﻿Shader "Unity Shaders Book/Chapter 15/Dissolve"
{
    Properties
    {
        // 控制消融的程度，值为0时没有消融，值为1时完全消融
        _BurnAmount ("Burn Amount", Range(0.0, 1.0)) = 0.0
        // 控制灼烧时边缘线宽，值越大，火焰边缘的蔓延范围越广
        _LineWidth ("Blur Line Width", Range(0.0, 0.2)) = 0.1
        // 物体原本的漫反射纹理
        _MainTex ("Base (RGB)", 2D) = "white" {}
        // 物体原本的法线纹理
        _BumpMap ("Normal Map", 2D) = "bump" {}
        // 火焰边缘颜色1
        _BurnFirstColor ("Burn First Color", Color) = (1.0, 0.0, 0.0, 1.0)
        // 火焰边缘颜色2
        _BurnSecondColor ("Burn Second Color", Color) = (1.0, 0.0, 0.0, 1.0)
        // 消融效果的噪声纹理
        _BurnMap ("Burn Map", 2D) = "white" {}
    }

    SubShader
    {
        Pass
        {
            // 为了得到正确的光照，设置Pass的LightMode和multi_compile_fwdbase编译指令
            Tags
            {
                "LightMode" = "ForwardBase"
            }

            // 双面渲染
            Cull Off

            CGPROGRAM

            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            #pragma multi_compile_fwdbase

            #pragma vertex vert
            #pragma fragment frag

            struct a2v
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uvMainTex : TEXCOORD0;
                float2 uvBumpMap : TEXCOORD1;
                float2 uvBurnMap : TEXCOORD2;
                float3 lightDir : TEXCOORD3;
                float3 worldPos : TEXCOORD4;
                // 阴影纹理坐标
                SHADOW_COORDS(5)
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _BumpMap;
            float4 _BumpMap_ST;
            sampler2D _BurnMap;
            float4 _BurnMap_ST;
            float _BurnAmount;
            float _LineWidth;
            fixed4 _BurnFirstColor;
            fixed4 _BurnSecondColor;

            v2f vert (a2v v)
            {
                v2f o;

                o.pos = UnityObjectToClipPos(v.vertex);
                // 计算三张纹理的UV
                o.uvMainTex = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.uvBumpMap = TRANSFORM_TEX(v.texcoord, _BumpMap);
                o.uvBurnMap = TRANSFORM_TEX(v.texcoord, _BurnMap);

                TANGENT_SPACE_ROTATION;
                // 将光源方向从模型空间变换到切线空间
                o.lightDir = mul(rotation, ObjSpaceLightDir(v.vertex)).xyz;

                // 为了得到阴影信息，计算世界空间下的顶点位置
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
                // 为了得到阴影信息，计算阴影纹理的采样坐标
                TRANSFER_SHADOW(o);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // 消融噪声纹理采样
                fixed3 burn = tex2D(_BurnMap, i.uvBurnMap).rgb;

                // 阈值以下的r分量被裁减掉，不会显示在屏幕上（被烧没的效果）
                clip(burn.r - _BurnAmount);

                // 切线空间光源方向
                float3 tangentLightDir = normalize(i.lightDir);
                // 切线空间法线方向（法线纹理本来就是基于切线空间）
                fixed3 tangentNormal = UnpackNormal(tex2D(_BumpMap, i.uvBumpMap));

                // 漫反射纹理采样得到反射率
                fixed3 albedo = tex2D(_MainTex, i.uvMainTex).rgb;
                // 环境光
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;
                // 基于切线空间的法线、光源方向点乘，求得漫反射光照
                // （因为这里用了法线纹理，所以需要基于切线空间，和之前基于世界空间的做法不同）
                fixed3 diffuse = _LightColor0.rgb * albedo * max(0, dot(tangentNormal, tangentLightDir));

                // 在宽度_LineWidth的范围内模拟一个烧焦的颜色变化
                // 计算得到混合系数t
                // 当t=1时，表明该像素位于消融的边界处
                // 当t=0时，表明该像素为未开始消融的地方
                fixed t = 1 - smoothstep(0.0, _LineWidth, burn.r - _BurnAmount);
                // 根据消融程度，得到烧焦的颜色
                fixed3 burnColor = lerp(_BurnFirstColor, _BurnSecondColor, t);
                // 使用pow函数对颜色进行处理，使之更接近烧焦的效果
                burnColor = pow(burnColor, 5);

                // 光照衰减
                UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);
                // 根据烧焦颜色，在正常漫反射（+环境光）和烧焦颜色之间进行插值
                // 使用step函数，保证当_BurnAmount为0时，不显示任何消融效果
                fixed3 finalColor = lerp(ambient + diffuse * atten, burnColor, t * step(0.0001, _BurnAmount));

                return fixed4(finalColor, 1.0);
            }
            ENDCG
        }

        // 定义一个用于投射阴影的Pass
        // 因为被消融掉的像素不应该投射阴影，所以不用普通的阴影Pass
        Pass
        {
            Tags
            {
                "LightMode" = "ShadowCaster"
            }

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_shadowcaster

            #include "UnityCG.cginc"

            struct v2f
            {
                // 内置宏，阴影投射内置封装的变量
                V2F_SHADOW_CASTER;
                float2 uvBurnMap : TEXCOORD1;
            };

            sampler2D _BumpMap;
            float4 _BumpMap_ST;
            sampler2D _BurnMap;
            float4 _BurnMap_ST;
            float _BurnAmount;

            v2f vert(appdata_base v)
            {
                v2f o;

                // 内置宏，阴影投射内置封装的逻辑，在里面会使用名称“v”作为输入的变量，
                // 所以顶点着色器的传参变量名必须是“v”
                // 且需要包含顶点位置v.vertex和顶点法线v.normal
                // 这里内置结构体appdata_base已经包含了这些变量
                // 如果需要做顶点动画，只需要在调用这个内置宏前修改v.vertex即可
                TRANSFER_SHADOW_CASTER_NORMALOFFSET(o);

                o.uvBurnMap = TRANSFORM_TEX(v.texcoord, _BurnMap);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed3 burn = tex2D(_BurnMap, i.uvBurnMap).rgb;

                // 被消融掉的像素不会投射阴影
                clip(burn.r - _BurnAmount);

                // 内置宏，阴影投射内置封装的逻辑
                SHADOW_CASTER_FRAGMENT(i)
            }

            ENDCG
        }
    }

    Fallback "Diffuse"
}

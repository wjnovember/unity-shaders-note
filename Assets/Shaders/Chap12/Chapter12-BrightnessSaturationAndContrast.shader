﻿Shader "Unity Shaders Book/Chapter 12/BrightnessSaturationAndContrast"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        // 下面的属性可以省略，因为我们在脚本中设置属性，无需在材质面板上调整
        _Brightness ("Brightness", Float) = 1
        _Saturation ("Saturation", Float) = 1
        _Contrast ("Contrast", Float) = 1
    }
    SubShader
    {
        Pass
        {
            // 屏幕后处理Shader的“标配”，保证后处理的效果能够写入缓冲区
            ZTest Always
            Cull Off
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            half _Brightness;
            half _Saturation;
            half _Contrast;

            // 使用内置的appdata_img结构体，在UnityCG.cginc中声明，
            // 只包含图像处理时必须的顶点坐标和纹理坐标等变量
            v2f vert (appdata_img v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }
            
            // 在片元着色器中调整亮度、饱和度、对比度
            fixed4 frag (v2f i) : SV_Target
            {
                // 屏幕图像采样
                fixed4 renderTex = tex2D(_MainTex, i.uv);
                
                // 亮度
                fixed3 finalColor = renderTex.rgb * _Brightness;

                // 饱和度
                // 计算亮度值：通过对每个颜色的分量乘以一个特定的系数，再相加得到
                fixed luminance = 0.2125 * renderTex.r + 0.7154 * renderTex.g + 0.721 * renderTex.b;
                // 得到饱和度为0的颜色值
                fixed3 luminanceColor = fixed3(luminance, luminance, luminance);
                // 通过_Saturation属性进行插值，得到调整饱和度后的颜色
                finalColor = lerp(luminanceColor, finalColor, _Saturation);

                // 对比度
                // 创建一个对比度为0的颜色值
                fixed3 avgColor = fixed3(0.5, 0.5, 0.5);
                // 通过_Contrast属性进行插值，得到调整对比度后的颜色值
                finalColor = lerp(avgColor, finalColor, _Contrast);

                return fixed4(finalColor, renderTex.a);

            }
            ENDCG
        }
    }

    // 关闭Fallback
    Fallback Off
}

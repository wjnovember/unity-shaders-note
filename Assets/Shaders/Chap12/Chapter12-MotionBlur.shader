﻿Shader "Unity Shaders Book/Chapter 12/Motion Blur"
{
    Properties
    {
        // 主纹理
        _MainTex ("Base (RGB)", 2D) = "white" {}
        // 混合图片时使用的混合系数
        _BlurAmount ("Blur Amount", Float) = 1.0
    }

    SubShader
    {
        CGINCLUDE

        #include "UnityCG.cginc"
        
        sampler2D _MainTex;
        fixed _BlurAmount;

        struct v2f
        {
            float4 pos : SV_POSITION;
            half2 uv : TEXCOORD0;
        };

        v2f vert(appdata_img v)
        {
            v2f o;
            o.pos = UnityObjectToClipPos(v.vertex);
            o.uv = v.texcoord;
            return o;
        }

        // 对当前图像进行采样，将其A通道的值设为_BlurAmount，方便后面混合时控制透明通道
        fixed4 fragRGB(v2f i) : SV_Target
        {
            return fixed4(tex2D(_MainTex, i.uv).rgb, _BlurAmount);
        }

        // 直接返回采样结果
        half4 fragA(v2f i) : SV_Target
        {
            return tex2D(_MainTex, i.uv);
        }

        ENDCG

        // 后处理“标配”
        ZTest Always
        Cull Off
        ZWrite Off

        // 第一个Pass，更新渲染纹理的RGB通道，
        // 通过_BlurAmount设置A通道来混合图像，但不会把A通道写入渲染纹理
        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            ColorMask RGB

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment fragRGB

            ENDCG
        }

        // 用当前图像的透明通道作为混合后的图像的透明通道
        Pass
        {
            Blend One Zero
            ColorMask A

            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment fragA

            ENDCG
        }
    }

    Fallback Off
}

﻿Shader "Unity Shaders Book/Chapter 12/Edge Detection"
{
    Properties
    {
        // 主纹理
        _MainTex ("Base (RGB)", 2D) = "white" {}
        // 控制边缘显示程度
        _EdgeOnly ("Edge Only", Float) = 1.0
        // 边缘颜色
        _EdgeColor ("Edge Color", Color) = (0, 0, 0, 1)
        // 背景色
        _BackgroundColor ("Background Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Pass
        {
            // 后处理“标配”，保证后处理的效果能够写入缓冲区
            ZTest Always
            Cull Off
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float4 pos : SV_POSITION;
                half2 uv[9] : TEXCOORD0;
            };

            sampler2D _MainTex;
            // 主纹理的纹素大小（例如：一张512 * 512的纹理，纹素大小为1/512）
            // 利用纹素，做相邻区域内纹理采样时，计算各相邻区域的纹理坐标
            half4 _MainTex_TexelSize;
            fixed _EdgeOnly;
            fixed4 _EdgeColor;
            fixed4 _BackgroundColor;

            // 计算明度
            fixed luminance(fixed4 color)
            {
                return 0.2125 * color.r + 0.7154 * color.g + 0.0721 * color.b;
            }

            half Sobel(v2f i)
            {
                // 定义水平方向使用的卷积核Gx
                const half Gx[9] = {
                    -1, -2, -1,
                    0, 0, 0,
                    1, 2, 1
                };

                // 定义竖直方向使用的卷积核Gy
                const half Gy[9] = {
                    -1, 0, 1,
                    -2, 0, 2,
                    -1, 0, 1
                };

                half texColor;
                half edgeX = 0;
                half edgeY = 0;

                for (int it = 0; it < 9; it++)
                {
                    // 计算明度值
                    texColor = luminance(tex2D(_MainTex, i.uv[it]));
                    //将明度值与卷积核Gx、Gy中对应的权重相乘后，叠加到各自的梯度值上
                    edgeX += texColor * Gx[it];
                    edgeY += texColor * Gy[it];
                }

                // 用1减去水平、竖直方向的梯度值的绝对值，得到edge
                half edge = 1 - abs(edgeX) - abs(edgeY);

                return edge;
            }

            // 使用内置的appdata_img结构体，在UnityCG.cginc中声明，
            // 只包含图像处理时必须的顶点坐标和纹理坐标等变量
            v2f vert (appdata_img v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);

                half2 uv = v.texcoord;

                // 获得周围一圈纹理坐标，用于Sobel算子采样需要
                o.uv[0] = uv + _MainTex_TexelSize.xy * half2(-1, -1);
                o.uv[1] = uv + _MainTex_TexelSize.xy * half2(0, -1);
                o.uv[2] = uv + _MainTex_TexelSize.xy * half2(1, -1);
                o.uv[3] = uv + _MainTex_TexelSize.xy * half2(-1, 0);
                o.uv[4] = uv + _MainTex_TexelSize.xy * half2(0, 0);
                o.uv[5] = uv + _MainTex_TexelSize.xy * half2(1, 0);
                o.uv[6] = uv + _MainTex_TexelSize.xy * half2(-1, 1);
                o.uv[7] = uv + _MainTex_TexelSize.xy * half2(0, 1);
                o.uv[8] = uv + _MainTex_TexelSize.xy * half2(1, 1);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // 调用Sobel函数计算当前像素的梯度值edge
                half edge = Sobel(i);

                // 利用梯度值edge，计算背景为原图和纯色下的颜色值
                fixed4 withEdgeColor = lerp(_EdgeColor, tex2D(_MainTex, i.uv[4]), edge);
                // 利用梯度值edge，计算_EdgeColor和背景色的颜色值
                fixed4 onlyEdgeColor = lerp(_EdgeColor, _BackgroundColor, edge);
                return lerp(withEdgeColor, onlyEdgeColor, _EdgeOnly);
            }
            ENDCG
        }
    }

    // 关闭Fallback
    Fallback Off
}

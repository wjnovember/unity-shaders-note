﻿Shader "Unity Shaders Book/Chapter 5/Simple Shader Property"
{
    Properties
    {
        // 声明一个Color类型的属性
        _Color("Color Tint", Color) = (1.0, 1.0, 1.0, 1.0)
    }

        SubShader
    {
        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            // CG代码中，若需要使用属性数值，需要声明变量，变量名与属性名一致，一般以下划线开头
            fixed4 _Color;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                fixed3 color : COLOR0;
            };

            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                
                o.color = v.normal * 0.5 + fixed3(0.5, 0.5, 0.5);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed3 color = i.color;
                // 使用_Color属性来影响法线转换成的颜色
                color *= _Color.rgb;
                return fixed4(color, 1.0);
            }

            ENDCG
        }
    }
}
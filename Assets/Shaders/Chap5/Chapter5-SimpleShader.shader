﻿// 第一行，通过Shader语义，定义这个Shader的名字，名字中使用'/'可定义该Shader的路径（或分组）
// 回到之前创建的材质，选择Shader，可以看到多了一个Unity Shaders Book目录，下面的子目录Chapter 5里面就有我们目前的Shader
Shader "Unity Shaders Book/Chapter 5/Simple Shader"
{
    // Properties语义并不是必需的

    SubShader
    {
        // SubShader中没有进行任何渲染设置和标签设置，
        // 所以该SubShader将使用默认的设置

        Pass
        {
            // Pass中没有进行任何渲染设置和标签设置，
            // 所以该Pass将使用默认的设置

            // 由 CGPROGRAM 和 ENDCG 包围 CG 代码片段
            CGPROGRAM

            // 告诉Unity，顶点着色器的代码在 vert 函数中 （格式：#pragma vertex [name]）
            #pragma vertex vert
            // 告诉Unity，片元着色器的代码在 frag 函数中 （格式：#pragma fragment [name]）
            #pragma fragment frag

            // 顶点着色器代码
            // 通过 POSITION 语义，告诉顶点着色器，输入v是这个顶点的位置
            // SV_POSITION语义表示返回的float4类型的变量，是当前顶点在裁剪空间中的位置
            // 注意：这两个语义是不能省略的，着色器通过语义来辨识各个变量是用来做什么的，并用在不同的底层处理中
            float4 vert(float4 v : POSITION) : SV_POSITION
            {
                return UnityObjectToClipPos(v);
            }

            // 片元着色器代码
            // 通过SV_Target语义，告诉渲染器，把用户的输出颜色存储到一个渲染目标中（比如：帧缓存中）
            // 颜色的RGBA每个分量范围在[0, 1]，所以使用fixed4类型
            // (0, 0, 0)表示黑色，(1, 1, 1)表示白色
            fixed4 frag() : SV_Target
            {
                return fixed4(1.0, 1.0, 1.0, 1.0);
            }

            ENDCG
        }
    }
}
﻿// 为Shader命名
Shader "Unity Shaders Book/Chapter 7/Ramp Texture"
{
    Properties
    {
        _Color ("Color Tint", Color) = (1, 1, 1, 1)
        // 渐变纹理
        _RampTex ("Ramp Tex", 2D) = "white" {}
        _Specular ("Specular", Color) = (1, 1, 1, 1)
        _Gloss ("Gloss", Range(8.0, 256)) = 20
    }

    SubShader
    {
        Pass
        {
            // 定义当前Pass在Unity的光照流水线中的角色
            Tags { "LightMode" = "ForwardBase" }

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            // 包含内置的Unity文件，可使用光照相关的内置变量、函数
            #include "Lighting.cginc"

            fixed4 _Color;
            // 声明渐变纹理的变量
            sampler2D _RampTex;
            // 声明渐变纹理的偏移和缩放（每个纹理变量都可使用这个变量：纹理名称+"_ST"）
            float4 _RampTex_ST;
            fixed3 _Specular;
            float _Gloss;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                // 纹理坐标
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 worldNormal : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
                float2 uv : TEXCOORD2;
            };

            v2f vert (a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);

                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                // 使用TRANSFORM_TEX宏来计算经过平铺、偏移后的纹理坐标
                o.uv = TRANSFORM_TEX(v.texcoord, _RampTex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed3 worldNormal = normalize(i.worldNormal);
                fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));

                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz;

                // 根据半兰伯特模型，通过法线方向和光照方向的点击做一次0.5倍的缩放以及一个0.5大小的偏移
                // 来计算半兰伯特部分halfLambert，得到的值范围被映射到[0, 1]之间
                fixed halfLambert = 0.5 * dot(worldNormal, worldLightDir) + 0.5;
                // 根据halfLambert构建一个纹理坐标，并使用这个纹理坐标对渐变纹理_RampTex进行采样
                // 由于_RampTex实际是一个一维纹理（纵轴方向颜色不变），因此纹理坐标的u和v方向都是用halfLambert
                // 采样得到的颜色和材质颜色_Color相乘，得到最终的漫反射颜色
                fixed3 diffuseColor = tex2D(_RampTex, fixed2(halfLambert, halfLambert)).rgb * _Color.rgb;
                fixed3 diffuse = _LightColor0.rgb * diffuseColor;

                // Blinn-Phong光照模型计算高光反射
                fixed3 viewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
                fixed3 halfDir = normalize(worldLightDir + viewDir);
                fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(max(0, dot(worldNormal, halfDir)), _Gloss);
                
                return fixed4(ambient + diffuse + specular, 1.0);
            }
            ENDCG
        }
    }

    // 使用内置的高光反射Shader兜底
    Fallback "Specular"
}

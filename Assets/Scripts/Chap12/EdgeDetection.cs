﻿using UnityEngine;

// 继承后处理基类
public class EdgeDetection : PostEffectsBase
{
    // 声明边缘检测Shader
    public Shader edgeDetectShader;
    // 声明边缘检测材质
    private Material edgeDetectMaterial = null;
    public Material material
    {
        get
        {
            edgeDetectMaterial = CheckShaderAndCreateMaterial(edgeDetectShader, edgeDetectMaterial);
            return edgeDetectMaterial;
        }
    }

    // edgesOnly为0：边缘将叠加在原渲染图像上
    // edgesOnly为1：只显示边缘，不显示渲染图像
    [Range(0.0f, 1.0f)]
    public float edgeOnly = 0.0f;
    // 边缘颜色
    public Color edgeColor = Color.black;
    // 背景颜色
    public Color backgroundColor = Color.white;

    // 将边缘参数传入材质，交由Shader处理图像
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material != null)
        {
            material.SetFloat("_EdgeOnly", edgeOnly);
            material.SetColor("_EdgeColor", edgeColor);
            material.SetColor("_BackgroundColor", backgroundColor);

            Graphics.Blit(source, destination, material);
        }
        else
        {
            Graphics.Blit(source, destination);
        }
    }
}
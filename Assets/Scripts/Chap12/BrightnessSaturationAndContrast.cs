﻿using UnityEngine;

// 继承后处理基类：PostEffectBase
public class BrightnessSaturationAndContrast : PostEffectsBase
{
    // 实现后处理的Shader
    public Shader briSatConShader;
    // 实现后处理的材质
    public Material briSatConMaterial;
    public Material material
    {
        get
        {
            briSatConMaterial = CheckShaderAndCreateMaterial(briSatConShader, briSatConMaterial);
            return briSatConMaterial;
        }
    }

    // 可调整的亮度
    [Range(0.0f, 3.0f)]
    public float brightness = 1.0f;
    // 可调整的饱和度
    [Range(0.0f, 3.0f)]
    public float saturation = 1.0f;
    // 可调整的对比度
    [Range(0.0f, 3.0f)]
    public float contrast = 1.0f;

    // 将对比度、饱和度、对比度传入材质属性，在Shader中调整屏幕图像
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material != null)
        {
            material.SetFloat("_Brightness", brightness);
            material.SetFloat("_Saturation", saturation);
            material.SetFloat("_Contrast", contrast);

            Graphics.Blit(source, destination, material);
        }
        else
        {
            Graphics.Blit(source, destination);
        }
    }
}
